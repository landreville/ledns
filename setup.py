from setuptools import setup, find_packages

setup(
    name='ledns',
    version=0.1,
    description="Automatic DNS verification for Let's Encrypt",
    keywords='',
    author='',
    author_email='',
    url='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'twisted',
    ],
)
