import argparse
import textwrap
from datetime import datetime
from zope.interface import provider
from twisted.logger import ILogObserver, formatEvent
from twisted.internet import reactor, defer
from twisted.names import dns, error, server
from twisted.logger import Logger


@provider(ILogObserver)
def simple_observer(event):
    print(
        f'{datetime.now().isoformat()} - ' 
        f'{event["log_level"].name.upper()} - '
        f'{formatEvent(event)}'
    )


log = Logger(observer=simple_observer)


class UntrackedName(Exception):
    pass


class DynamicResolver(object):

    base_domain = b'letsencrypt.example.com'
    verify_domains = {b'whatever.letsencrypt.example.com': b'TESTTXTVALUE'}
    ip = b'127.0.0.9'

    def _respond(self, query):
        name = query.name.name
        log.debug(f'DNS request for: {dns.QUERY_TYPES[query.type]} {name}')
        if (query.type == dns.A and
            (name == self.base_domain or name in self.verify_domains)
        ):
            answer = dns.RRHeader(
                name=name,
                payload=dns.Record_A(address=self.ip)
            )

        elif query.type == dns.TXT and name in self.verify_domains:
            answer = dns.RRHeader(
                name=name,
                type=dns.TXT,
                payload=dns.Record_TXT(self.verify_domains[name])
            )

        else:
            log.warn(f'Invalid request for: {dns.QUERY_TYPES[query.type]} {name}')
            raise UntrackedName(f'Invalid name "{name}"')

        answers = [answer]
        authority = []
        additional = []
        return answers, authority, additional

    def query(self, query, _timeout=None):
        try:
            return defer.succeed(self._respond(query))
        except UntrackedName as e:
            return defer.fail(error.DomainError())


def main(config_file):
    factory = server.DNSServerFactory(clients=[DynamicResolver()])
    protocol = dns.DNSDatagramProtocol(controller=factory)

    reactor.listenUDP(10053, protocol)
    reactor.listenTCP(10053, factory)

    reactor.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        help=textwrap.dedent("""\
            Renew let's encrypt certificate with DNS.

            Configuration file example:

            base_domain: letsencrypt.example.com
            verify_domains:
                - whatever.letsencrypt.example.com
                - another.letsencrypt.example.com
            """)
    )

    parser.add_argument(
        '--run-forever',
        action='store_true',
        default=False,
        help='Keep DNS server running. Default is to quit after verification '
             'DNS request is made.'
    )

    args = vars(parser.parse_args())
    main(**args)
